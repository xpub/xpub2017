all: index.json

archive.json:
	python scripts/mediawiki.py gallery --name archive --recursive \
		https://pzwiki.wdka.nl/mediadesign/Category:2016 \
		https://pzwiki.wdka.nl/mediadesign/Category:2015 \
		https://pzwiki.wdka.nl/mediadesign/Category:2014 \
		https://pzwiki.wdka.nl/mediadesign/Category:2013 \
		https://pzwiki.wdka.nl/mediadesign/Category:2012 \
		https://pzwiki.wdka.nl/mediadesign/Category:2011 \
		https://pzwiki.wdka.nl/mediadesign/Category:2010 \
		https://pzwiki.wdka.nl/mediadesign/Category:2009 \
		https://pzwiki.wdka.nl/mediadesign/Category:2008 \
		https://pzwiki.wdka.nl/mediadesign/Category:2007 \
		https://pzwiki.wdka.nl/mediadesign/Category:2006 \
		https://pzwiki.wdka.nl/mediadesign/Category:2005 \
		https://pzwiki.wdka.nl/mediadesign/Category:2004 > archive.json

drop.node.json: drop.json
	cat drop.json | python scripts/leaflet.py gallery --recursive --direction 2 > drop.node.json

about.json: about.txt
	python scripts/texthierarchy.py < about.txt > about.json

index.json: archive.json about.json drop.node.json
	python scripts/includenodes.py xpub.top.json > index.json


Installation
=============

Make sure your virtualenv is up to date (had troubles with html5lib).

Create and activate your venv.

     virtualenv venv
     source venv/bin/activate

Install python dependencies with pip

     pip install pillow html5lib mwclient


from __future__ import print_function
from html5lib import parse
import sys, json
from xml.etree import ElementTree as ET 


def process (f):
    stack = []
    for line in f:
        line = line.rstrip()
        if line:
            level = 0
            while line.startswith("\t"):
                line = line[1:]
                level += 1
            print (level, line, file=sys.stderr)
            node = {
                'text': line,
                'level': level,
                'children': []
            }
            while len(stack) > level:
                stack.pop()
            if len(stack):
                stack[len(stack)-1]['children'].append(node)
            stack.append(node)
    return stack[0]

if __name__ == "__main__":
    n = process(sys.stdin)
    import json
    print (json.dumps(n, indent=2))
from __future__ import print_function
from argparse import ArgumentParser

ap = ArgumentParser("")
ap.add_argument("input")
args = ap.parse_args()

import json

with open(args.input) as f:
	node = json.load(f)

def expand (node):
	if node == None:
		return node
	retnode = node
	if "@include" in node:
		with open(node['@include']) as f:
			retnode = json.load(f)
		if "text" in node:
			retnode['text'] = node['text']
	if "children" in retnode:
		retnode['children'] = [expand(c) for c in retnode['children']]

	return retnode

print (json.dumps(expand(node), indent=2))

